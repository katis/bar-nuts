#![feature(test)]
extern crate term;
extern crate test;

mod bar_printer;
mod square;

use std::env;
use bar_printer::BarPrinter;
use square::find_largest_square;

fn main() {
    let heights: Vec<usize> = env::args()
        .nth(1)
        .iter()
        .flat_map(|arg| arg.split(","))
        .map(|num| num.parse::<usize>().unwrap())
        .collect();

    let results = find_largest_square(&heights);

    for square in results {
        println!("");
        println!("   Size is {}", square.size);
        println!("");
        let mut printer = BarPrinter::new(square, &heights);
        match printer.print() {
            Err(error) => println!("Failed to print bars: {:?}", error),
            _ => {}
        }
    }
}

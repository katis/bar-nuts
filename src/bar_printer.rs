use std::io;
use term;
use term::StdoutTerminal;

use square::Square;

pub struct BarPrinter<'a> {
    height: usize,
    data: &'a [usize],
    square: Square,
    term: Box<StdoutTerminal>,
}

enum Block {
    Square,
    Bar,
    Empty
}

impl<'a> BarPrinter<'a> {
    pub fn new(square: Square, data: &'a [usize]) -> BarPrinter {
        let height = *data.iter().max().unwrap_or(&0);
        BarPrinter {
            height: height,
            data: data,
            square: square,
            term: term::stdout().unwrap(),
        }
    }

    fn block(&self, x: usize, inverse_y: usize, square_end_x: usize) -> Block {
        if (x >= self.square.start_index && x < square_end_x) && (inverse_y <= self.square.size) {
            Block::Square
        } else if inverse_y <= self.data[x] {
            Block::Bar
        } else {
            Block::Empty
        }
    }

    pub fn print(&mut self) -> io::Result<()> {
        let square_end_x = self.square.start_index + self.square.size;
        for y in 0..self.height {
            self.print_prefix()?;
            let inverse_y = self.height - y;
            for x in 0..self.data.len() {
                let block = self.block(x, inverse_y, square_end_x);
                match block {
                    Block::Square => self.print_square(),
                    Block::Bar => self.print_bar(),
                    Block::Empty => self.print_empty(),
                }?;
            }
            self.print_eol()?;
        }
        self.reset_colors()?;
        Ok(())
    }

    fn print_prefix(&mut self) -> io::Result<()> {
        self.reset_colors()?;
        write!(self.term, "   ")
    }

    fn print_square(&mut self) -> io::Result<()> {
        self.term.fg(term::color::WHITE)?;
        self.term.bg(term::color::GREEN)?;
        write!(self.term, "[]")
    }

    fn print_bar(&mut self) -> io::Result<()> {
        self.term.fg(term::color::GREEN)?;
        self.term.bg(term::color::WHITE)?;
        write!(self.term, "[]")
    }

    fn print_empty(&mut self) -> io::Result<()> {
        self.term.bg(term::color::BLACK)?;
        write!(self.term, "  ")
    }

    fn print_eol(&mut self) -> io::Result<()> {
        self.reset_colors()?;
        writeln!(self.term, "")
    }

    fn reset_colors(&mut self) -> io::Result<()> {
        self.term.fg(term::color::WHITE)?;
        self.term.bg(term::color::BLACK)?;
        Ok(())
    }
}


Prints the largest square found in a group of columns:

![example](example.png)